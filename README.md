### Description
This app is used for Redux workshop. This app will serve as the API.

### How to start
Terminal: Run `./gradlew bootRun`
This App will run on port:8080 by default
